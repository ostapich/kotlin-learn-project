package com.hdmytro.allaboutapps.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.hdmytro.allaboutapps.R
import com.hdmytro.allaboutapps.commons.api.models.Club
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_view_club.*
import kotlinx.android.synthetic.main.toolbar.*
import android.content.Intent
import android.view.View
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan

class ViewClub : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_club)
        setSupportActionBar(toolbar)
        var club : Club = intent.getParcelableExtra("CLUB")

        var actionBar = getSupportActionBar()
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.title = club.name
        }

        toolbar.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                startActivity(Intent(applicationContext, MainActivity::class.java))
                finish()
            }
        })

        club_country_title.text = club.country
        var bodyTest = String.format(
            getResources().getString(R.string.club_value_text), club.name, club.country, club.value.toString()
        )
        club_valuable.text =  setBoldText(bodyTest, club.name)

        Picasso
            .get()
            .load(club.image)
            .placeholder(R.drawable.club_placeholder)
            .into(club_logo);
    }

    fun setBoldText(description : String, clubName : String) : SpannableStringBuilder {
        val resultText = SpannableStringBuilder(description);
        val positionStart = description.indexOf(clubName)
        val positionEnd = clubName.length + positionStart

        var bss = StyleSpan(android.graphics.Typeface.BOLD);
        resultText.setSpan(bss, positionStart, positionEnd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return resultText
    }
}
