package com.hdmytro.allaboutapps.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.hdmytro.allaboutapps.R
import com.hdmytro.allaboutapps.commons.api.Access
import com.hdmytro.allaboutapps.commons.api.models.Club
import com.hdmytro.allaboutapps.commons.recycler.ClubAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private var mClubs : List<Club>? = null

    private var mDecentSort : Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        loadPartsAndUpdateList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }


    private fun loadPartsAndUpdateList() {
        club_list.layoutManager = LinearLayoutManager(this)
        val context = this
        loader.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            delay(1000)
            val clubResponce = Access.clubApi.getClubAsync().await()
            if (clubResponce.isSuccessful) {

                mClubs = clubResponce.body()
                val clubList : List<Club>? = sortClub(mClubs)

                if (clubList != null) {
                    loader.visibility = View.GONE
                    club_list.apply {
                        adapter = ClubAdapter(clubList, getResources(), { club : Club -> clubClicked(club) })
                        adapter.notifyDataSetChanged()
                    }
                }
            } else {
                Toast.makeText(context, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun clubClicked(club : Club) : Unit {
        val intent = Intent(this, ViewClub::class.java)
        intent.putExtra("CLUB", club)
        startActivity(intent)
    }

    fun sortClub(clubList : List<Club>?, sortDecentDirection : Boolean = false) : List<Club>? {
        var resultList : List<Club>? = null
        if (clubList != null) {
            if (sortDecentDirection == false) {
                resultList = clubList.sortedWith(compareBy { it.name })
            } else {
                resultList = clubList.sortedByDescending { it.value  }
            }
        }
        return resultList
    }

    @Override
    override  fun onOptionsItemSelected(item : MenuItem) : Boolean {
        val id = item.itemId
        if (id == R.id.menuSort) {
            sortItems()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun sortItems() : Unit {
        val clubList = sortClub(mClubs, mDecentSort)
        changeDirectionSort()
        if (clubList != null) {
            club_list.apply {
                adapter = ClubAdapter(clubList, getResources(), { club : Club -> clubClicked(club) })
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun changeDirectionSort() : Unit {
        if (mDecentSort == false) {
            mDecentSort = true
        } else {
            mDecentSort = false
        }
    }

}
