package com.hdmytro.allaboutapps.commons.api

import com.hdmytro.allaboutapps.commons.api.client.ClubApiClient
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object Access {
    val clubApi : ClubApiClient by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://public.allaboutapps.at/")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

        return@lazy retrofit.create(ClubApiClient::class.java)
    }
}
