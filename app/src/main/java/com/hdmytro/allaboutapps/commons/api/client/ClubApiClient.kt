package com.hdmytro.allaboutapps.commons.api.client

import com.hdmytro.allaboutapps.commons.api.models.Club
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface ClubApiClient {
    @GET("hiring/clubs.json") fun getClubAsync(): Deferred<Response<List<Club>>>
}