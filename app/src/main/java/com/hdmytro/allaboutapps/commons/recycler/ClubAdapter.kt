package com.hdmytro.allaboutapps.commons.recycler

import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.hdmytro.allaboutapps.R
import com.hdmytro.allaboutapps.commons.api.models.Club
import com.squareup.picasso.Picasso

class ClubAdapter(private val list: List<Club>, val resource: Resources, val clickListener: (Club) -> Unit): RecyclerView.Adapter<ClubViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClubViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ClubViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ClubViewHolder, position: Int) {
        holder.bind(list[position], resource, clickListener)
    }

    override fun getItemCount(): Int = list.size
}

class ClubViewHolder(inflater: LayoutInflater, parent: ViewGroup):
    RecyclerView.ViewHolder(inflater.inflate(R.layout.recyclerview_club_item, parent, false)) {

    private var mPicture: ImageView? = null
    private var mClubName: TextView? = null
    private var mCountry: TextView? = null
    private var mClubValue: TextView? = null

    init {
        mPicture = itemView.findViewById(R.id.clubLogo)
        mClubName = itemView.findViewById(R.id.club_name)
        mCountry = itemView.findViewById(R.id.club_country)
        mClubValue = itemView.findViewById(R.id.club_value)
    }

    fun bind(club: Club, resource: Resources, clickListener: (Club) -> Unit) {
        mClubName?.text = club.name
        mCountry?.text = club.country
        mClubValue?.text = String.format(
            resource.getString(R.string.club_value), club.value.toString()
        )
        Picasso
            .get()
            .load(club.image)
            .placeholder(R.drawable.club_placeholder)
            .into(mPicture);
        itemView.setOnClickListener { clickListener(club)}
    }
}
