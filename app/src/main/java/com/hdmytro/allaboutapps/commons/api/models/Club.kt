package com.hdmytro.allaboutapps.commons.api.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Club(var name: String, val country: String, val value: Long, val image: String?) : Parcelable